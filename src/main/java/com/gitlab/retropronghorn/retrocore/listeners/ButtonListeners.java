package com.gitlab.retropronghorn.retrocore.listeners;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.RetroInventory;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ButtonListeners implements Listener {
    private RetroInventory retroInventory;

    /**
     * Create new button listener events
     *
     * @param inventory Inventory to listen for click events on
     **/
    public ButtonListeners(RetroInventory inventory) {
        this.retroInventory = inventory;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null) return;
        ItemStack clickedItem = event.getCurrentItem();
        Player player = (Player) event.getWhoClicked();

        // ----------------------
        // CLOSE BUTTON EVENTS
        // ----------------------
        if (clickedItem.equals(retroInventory.gui.buttonManager.closeButton.item)) {
            // Sounds
            if (retroInventory.gui.getOptions().get("enable_sounds"))
                player.playSound(
                        player.getLocation(),
                        Sound.valueOf(RetroCore.config.getString(retroInventory.gui.buttonManager.closeButton.configNode + ".sound")),
                        1f,
                        1f);
            Bukkit.getScheduler().runTaskLater(retroInventory.core, () -> {
                player.closeInventory();
            }, 1L);
        }

        // ----------------------
        // NEXT BUTTON EVENTS
        // ----------------------
        if (clickedItem.equals(retroInventory.gui.buttonManager.nextButton.item)) {
            retroInventory.menu.pageHandler(retroInventory.menu,"next");
            // Sounds
            if (retroInventory.gui.getOptions().get("enable_sounds"))
                player.playSound(
                        player.getLocation(),
                        Sound.valueOf(RetroCore.config.getString(retroInventory.gui.buttonManager.nextButton.configNode + ".sound")),
                        1f,
                        1f);
        }

        // ----------------------
        // PREV BUTTON EVENTS
        // ----------------------
        if (clickedItem.equals(retroInventory.gui.buttonManager.prevButton.item)) {
            retroInventory.menu.pageHandler(retroInventory.menu,"prev");
            // Sounds
            if (retroInventory.gui.getOptions().get("enable_sounds"))
                player.playSound(
                        player.getLocation(),
                        Sound.valueOf(RetroCore.config.getString(retroInventory.gui.buttonManager.prevButton.configNode + ".sound")),
                        1f,
                        1f);
        }

        // ----------------------
        // FILTER BUTTON EVENTS
        // ----------------------
        if (clickedItem.equals(retroInventory.gui.buttonManager.filterButton.item)) {
            // Update
            retroInventory.gui.nextFilterType();
            retroInventory.gui.buttonManager.filterButton.updateLore();
            // Hot-load new lore
            clickedItem.setLore(retroInventory.gui.buttonManager.filterButton.getLore());
            // Filter items and repopulate
            String filterType = retroInventory.gui.filterType;
            retroInventory.gui.getRetroInventory().filterItems(filterType);

            // Sounds
            if (retroInventory.gui.getOptions().get("enable_sounds"))
                    player.playSound(
                            player.getLocation(),
                            Sound.valueOf(RetroCore.config.getString(retroInventory.gui.buttonManager.filterButton.configNode + ".sound")),
                            1f,
                            1f);
        }
    }
}
