package com.gitlab.retropronghorn.retrocore.listeners;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.RetroInventory;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryListeners implements Listener {
    private RetroInventory retroInventory;

    /**
     * Create new Inventory listener events
     *
     * @param inventory Inventory to listen for interaction events on
     **/
    public InventoryListeners(RetroInventory inventory) {
        this.retroInventory = inventory;
    }

    @EventHandler
    public void onOpenInventory(InventoryOpenEvent event) {
        // ----------------------
        // Open Inventory Sounds
        // ----------------------
        if (event.getInventory().equals(retroInventory.getInventory())){
            Player player = (Player) event.getPlayer();
            if (retroInventory.gui.getOptions().get("enable_sounds"))
                player.playSound(
                        player.getLocation(),
                        Sound.valueOf(RetroCore.config.getString("gui.open_sound")),
                        1f,
                        1f);
        }
    }

    @EventHandler
    public void onCloseInventory(InventoryCloseEvent event) {
        // ----------------------
        // Cleanup Tasks
        // ----------------------
        if (!retroInventory.gui.getOptions().get("persistence"))
            retroInventory.populate(retroInventory.itemCache);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(retroInventory.getInventory())) return;
        if (event.getCurrentItem() == null) return;
        if (event.getCurrentItem().getItemMeta() == null) return;
        ItemStack clickedItem = event.getCurrentItem();

        // ----------------------
        // Cancel UI Events
        // ----------------------
        if (retroInventory.gui.getOptions().get("freeze_inventory")) {
            event.setCancelled(true);
            // Update inv to help prevent spam obtaining items.
            Player player = (Player) event.getWhoClicked();
            Bukkit.getScheduler().runTaskLater(retroInventory.core, () -> {
                player.updateInventory();
            }, 1L);
        }
    }
}
