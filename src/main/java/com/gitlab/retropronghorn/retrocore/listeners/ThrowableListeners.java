package com.gitlab.retropronghorn.retrocore.listeners;

import com.gitlab.retropronghorn.retrocore.throwables.ThrowableItem;
import com.gitlab.retropronghorn.retrocore.throwables.effects.HitHandler;
import com.gitlab.retropronghorn.retrocore.throwables.effects.ParticleHandler;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.projectiles.ProjectileSource;

public class ThrowableListeners implements Listener {
    private ThrowableItem throwableItem;

    /**
     * Create new Throwable listener events
     *
     * @param throwableItem Throwable item to bind events to
     **/
    public ThrowableListeners(ThrowableItem throwableItem) {
        this.throwableItem = throwableItem;
    }

    @EventHandler
    public void onThrowEntity(ProjectileLaunchEvent event) {
      if (event.getEntity() instanceof Snowball) {
          PersistentDataContainer data = ((Snowball) event.getEntity()).getItem().getItemMeta().getPersistentDataContainer();
          Integer isThrowable = data.get(throwableItem.throwableIdentifier, PersistentDataType.INTEGER);
          if (isThrowable != null && isThrowable == 1) {
              Entity throwableItem = (Snowball) event.getEntity();
              // Apply particle trail to item
              // TODO: Don't do this if the item doesn't have particle effects bound
              ParticleHandler particleTrail = new ParticleHandler();
              particleTrail.particleTrail(throwableItem);
          }
      }
    }

    @EventHandler
    public void onEntityHit(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Snowball) {
            // Get item persistent data
            ItemStack attackObject = ((Snowball) event.getDamager()).getItem();
            PersistentDataContainer data = attackObject.getItemMeta().getPersistentDataContainer();
            Integer isThrowable = data.get(throwableItem.throwableIdentifier, PersistentDataType.INTEGER);
            // If this is a Throwable, apply effects
            if (event.getEntity() instanceof Player && isThrowable != null && isThrowable == 1) {
                Player victim = (Player) event.getEntity();
                ProjectileSource attacker = ((Snowball) event.getDamager()).getShooter();
                String effects = data.get(throwableItem.effectsIdentifier, PersistentDataType.STRING);
                // Handle Throwable hit effects
                HitHandler hitHandler = new HitHandler();
                if (attacker instanceof Player) {
                    hitHandler.dispatchSounds(victim, (Player) attacker);
                    hitHandler.notifyPlayersInChat(victim, (Player) attacker, attackObject.getItemMeta().getDisplayName());
                }

                hitHandler.applyEffects(victim, effects);

                // Impact Particles
                ParticleHandler particleHandler = new ParticleHandler();
                String impactParticles = data.get(throwableItem.impactParticlesIdentifier, PersistentDataType.STRING);
                if (impactParticles != null)
                    particleHandler.playerHitParticle(throwableItem.core, victim, impactParticles);
            }
        }
    }
}
