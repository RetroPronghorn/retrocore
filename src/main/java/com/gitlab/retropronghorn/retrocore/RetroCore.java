package com.gitlab.retropronghorn.retrocore;

import com.gitlab.retropronghorn.retrocore.commands.GUIDebug;
import com.gitlab.retropronghorn.retrocore.commands.ThrowableDebug;
import com.gitlab.retropronghorn.retrocore.gui.GUI;
import com.gitlab.retropronghorn.retrocore.throwables.ThrowableItem;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public final class RetroCore extends JavaPlugin {
    public static RetroCore instance;
    public static FileConfiguration config;

    public static RetroCore getPlugin() {
        return instance;
    }

    @Override
    public void onEnable() {
        config = this.getConfig();
        // Set instance
        instance = this;
        // Plugin startup logic

        // Testing commands, remove after full 1.0 release
        // Objects.requireNonNull(this.getCommand("gui")).setExecutor(new GUIDebug());
        Objects.requireNonNull(this.getCommand("throwable")).setExecutor(new ThrowableDebug());

        // Load message
        log("Loaded RetroCore " + this.getDescription().getVersion() + " by " + this.getDescription().getAuthors());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    /**
     * Log a new line as RetroCore
     *
     * @param message The message to log to console
     */
    public static void log(String message) {
        System.out.println("[" + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "RetroCore" + ChatColor.RESET + "]: " + message);
    }

    /**
     * Create a new GUI
     *
     * @param name Name of the GUI, this will be used as the title
     * @param size default size of the inventory
     * @param options Options for this GUI
     */
    public static GUI createGUI(String name, Integer size, ArrayList<ItemStack> items, HashMap<String, Boolean> options) {
        return new GUI(instance, name, size, items, options);
    }

    /**
     * Create a new GUI
     *
     * @param name Name of the GUI, this will be used as the title
     * @param size default size of the inventory
     */
    public static GUI createGUI(String name, Integer size, ArrayList<ItemStack> items) {
        return new GUI(instance, name, size, items, new HashMap<>());
    }

    /**
     * Create a new throwable item
     *
     * @param name Name of the throwable item
     * @param description Description of the item
     **/
    public static ThrowableItem createThrowable(String name, String description) {
        return new ThrowableItem(instance, name, description);
    }
}
