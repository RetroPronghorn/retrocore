package com.gitlab.retropronghorn.retrocore.commands;

import com.gitlab.retropronghorn.retrocore.gui.GUI;
import com.gitlab.retropronghorn.retrocore.RetroCore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class GUIDebug implements CommandExecutor {
    private Inventory inv;

    public GUIDebug() {
        ArrayList<ItemStack> items = new ArrayList<>();
        int itemCount = 41;
        ArrayList<Material> materials = new ArrayList<>();
        materials.add(Material.BONE);
        materials.add(Material.COOKIE);
        materials.add(Material.POTATO);
        materials.add(Material.BREAD);
        materials.add(Material.APPLE);
        materials.add(Material.CARROT);
        materials.add(Material.BEETROOT);

        for(int i = 0; i < itemCount; i++) {
            int max = materials.size();
            int r = new Random().nextInt(max);
            ItemStack item = new ItemStack(materials.get(r));
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(materials.get(r).toString() + i);
            item.setItemMeta(meta);

            items.add(item);
        }

        // Create a GUI
        GUI newGUI = RetroCore.createGUI(
                ChatColor.BLACK + "" + ChatColor.BOLD + "Test GUI",
                9*3,
                items,
                new HashMap<String, Boolean>(){{
                    put("dynamic_rows", false);
                }});
        inv = newGUI.getRetroInventory().getInventory();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player senderPlayer = Bukkit.getPlayer(sender.getName());
        System.out.println(ChatColor.RED + "" + senderPlayer + " executed debug, if this is in production you've got a problem!");

        // Open GUI
        senderPlayer.openInventory(inv);
        return false;
    }
}
