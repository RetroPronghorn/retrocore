package com.gitlab.retropronghorn.retrocore.commands;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.throwables.ThrowableItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ThrowableDebug implements CommandExecutor {
    public ItemStack slowBall;

    public ThrowableDebug() {

        // Give Throwable
        ThrowableItem throwable = RetroCore.createThrowable("Freeze Ball", "Slow down other players!");
        HashMap<String, Integer> effects = new HashMap<String, Integer>(){{
            put("SLOW", 30);
            put("SLOW_FALLING", 30);
        }};
        // Register particles
        ArrayList<String> hitParticles = new ArrayList<>();
        hitParticles.add("ENDER_SIGNAL");
        hitParticles.add("DRAGON_BREATH");

        slowBall = throwable.spawn(1, effects, hitParticles);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player senderPlayer = Bukkit.getPlayer(sender.getName());
        System.out.println(ChatColor.RED + "" + senderPlayer + " executed debug, if this is in production you've got a problem!");

        // Count
        int amount = 1;
        if (args.length > 0) {
            amount = Integer.parseInt(args[0]);
        }

        for (int i = 0; i < amount; i++)
            senderPlayer.getInventory().addItem(slowBall);
        return false;
    }
}
