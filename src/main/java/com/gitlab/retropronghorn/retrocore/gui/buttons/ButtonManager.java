package com.gitlab.retropronghorn.retrocore.gui.buttons;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.GUI;

public class ButtonManager {
    public final PrevButton prevButton;
    public final NextButton nextButton;
    public final CloseButton closeButton;
    public final FilterButton filterButton;

    /**
     * Create new button manager
     *
     * @param core The core instance
     **/
    public ButtonManager(RetroCore core, GUI gui) {
        // Build Buttons
        closeButton = new CloseButton(core);
        nextButton = new NextButton(core);
        prevButton = new PrevButton(core);
        filterButton = new FilterButton(core, gui);
    }
}
