package com.gitlab.retropronghorn.retrocore.gui.buttons;

import com.gitlab.retropronghorn.retrocore.RetroCore;


public class CloseButton extends Button{
    /**
     * Create the close button
     *
     * @param core RetroCore instance
     */
    public CloseButton(RetroCore core) {
        super(core, "gui.close_button");
    }
}
