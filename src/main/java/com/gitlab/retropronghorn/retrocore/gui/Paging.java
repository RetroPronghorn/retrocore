package com.gitlab.retropronghorn.retrocore.gui;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.buttons.ButtonManager;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Paging {
    RetroInventory retroInventory;

    public Paging (RetroInventory inventory) {
        this.retroInventory = inventory;
    }

    /**
     * Recalculate menu in inventory
     *
     * @param menu The menu to add to the inventory
     * @param end Are we at the end of the paging?
     */
    public void recalcMenuNav(Menu menu, Boolean end) {
        ButtonManager buttonManager = retroInventory.gui.buttonManager;

        if (retroInventory.gui.getOptions().get("enable_paging")) {
            retroInventory.getInventory().setItem(
                    retroInventory.getInventory().getSize() - RetroCore.config.getInt("row_size") +
                        RetroCore.config.getInt(buttonManager.prevButton.configNode + ".menu_index"),
                    (retroInventory.page > 1) ? buttonManager.prevButton.item : menu.blankSlot);
            retroInventory.getInventory().setItem(
                    retroInventory.getInventory().getSize() - RetroCore.config.getInt("row_size") +
                            RetroCore.config.getInt(buttonManager.nextButton.configNode + ".menu_index"),
                    (!end) ? buttonManager.nextButton.item : menu.blankSlot);
        }
    }

    /**
     * Build a page in the gui
     *
     * @param menu The menu for this gui
     * @param remaining Remaining items in the inventory queue
     */
    private void buildPage(Menu menu, Integer remaining) {
        int usableSize = retroInventory.getInventory().getSize() - RetroCore.config.getInt("row_size");
        int start = usableSize * (retroInventory.page-1);
        int end = (remaining < usableSize) ? start + remaining : start + usableSize;
        List<ItemStack> page = retroInventory.items.subList(start, end);
        retroInventory.populatePage(page);
        recalcMenuNav(menu,remaining < usableSize);
    }

    /**
     * Handle page turn events
     *
     * @param menu The menu for this gui
     * @param direction Direction to page two
     */
    public void pageHandler(Menu menu, String direction) {
        int usableSize = retroInventory.getInventory().getSize() - RetroCore.config.getInt("row_size");
        int itemsRemaining;

        switch (direction) {
            case "next":
                itemsRemaining = retroInventory.items.size() - (usableSize * retroInventory.page);
                if (itemsRemaining > 0) {
                    retroInventory.setPage(retroInventory.page + 1);
                    buildPage(menu, itemsRemaining);
                }
                break;
            case "prev":
                itemsRemaining = retroInventory.items.size() + (usableSize * retroInventory.page);
                if (retroInventory.page > 1) {
                    retroInventory.setPage(retroInventory.page - 1);
                    buildPage(menu, itemsRemaining);
                }
                break;
            default:
                break;
        }

    }
}
