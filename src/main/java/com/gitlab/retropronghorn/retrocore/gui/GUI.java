package com.gitlab.retropronghorn.retrocore.gui;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.buttons.ButtonManager;
import com.gitlab.retropronghorn.retrocore.listeners.ButtonListeners;
import com.gitlab.retropronghorn.retrocore.listeners.InventoryListeners;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GUI {
    final public String name;
    public Integer size;
    public ButtonManager buttonManager;
    public final List<String> filterTypes = Arrays.asList("none", "alpha", "type" /*amount*/);
    public String filterType = filterTypes.get(0);


    // Default Options
    final private HashMap<String, Boolean> options = new HashMap<String, Boolean>() {{
        // Disallow removal and insertion of items
        put("freeze_inventory", true);
        // Attempt to size inventory based on number of items
        put("dynamic_rows", true);
        // Enable menu at bottom of GUI to close, and page
        put("enable_menu", true);
        // Available Menu Buttons
        put("close_button", true);
        // Enable paging for item overflow
        put("enable_paging", true);
        // Enable default sounds
        put("enable_sounds", true);
        // Enable filtering
        put("enable_basic_filter", true);
        // Enable persistence
        put("persistence", false);
    }};

    // Create a default inventory object
    private RetroInventory retroInventory;

    /**
     * Create a new GUI
     *
     * @param instance Instance to RetroCore
     * @param size default size of the inventory
     * @param optionsOverrides Options for the
     */
    public GUI(RetroCore instance, String name, Integer size, ArrayList<ItemStack> items, HashMap<String, Boolean> optionsOverrides) {
        this.name = name;
        this.size = size;

        // ----------------------
        // MERGE CONFIG OPTIONS
        // ----------------------
        options.putAll(optionsOverrides);

        // ----------------------
        // DETERMINE ROWS & SIZE
        // ----------------------
        if (options.get("dynamic_rows")) {
            double itemSize = items.size();
            double rowSize = RetroCore.config.getInt("row_size");
            double rows = Math.ceil(itemSize / rowSize);
            int maxSize = RetroCore.config.getInt("max_rows") * RetroCore.config.getInt("row_size");
            if (((int) rows) > RetroCore.config.getInt("max_rows")) {
                this.size = maxSize;
            } else {
                this.size = RetroCore.config.getInt("row_size") * (int) rows;
            }
        }

        // ----------------------
        // Build Inventory
        // ----------------------
        this.buttonManager = new ButtonManager(instance, this);
        retroInventory = new RetroInventory(instance, this, items);

        // ----------------------
        // REGISTER EVENTS
        // ----------------------
        instance.getServer().getPluginManager().registerEvents(new ButtonListeners(retroInventory), instance);
        instance.getServer().getPluginManager().registerEvents(new InventoryListeners(retroInventory), instance);
    }

    public RetroInventory getRetroInventory() {
        return this.retroInventory;
    }

    /**
     * Create a new GUI
     *
     * @return options returns current options & overrides
     */
    public HashMap<String, Boolean> getOptions() {
        return this.options;
    }

    public void nextFilterType() {
        int nextIndex = (filterTypes.indexOf(filterType)+1 > filterTypes.size()-1) ?
                0 : filterTypes.indexOf(filterType)+1;
        filterType = filterTypes.get(nextIndex);
    }
}
