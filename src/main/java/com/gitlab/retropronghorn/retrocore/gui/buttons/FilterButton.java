package com.gitlab.retropronghorn.retrocore.gui.buttons;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.GUI;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;


public class FilterButton extends Button{
    private String title;
    private GUI gui;
    /**
     * Create the close button
     *
     * @param core RetroCore instance
     */
    public FilterButton(RetroCore core, GUI gui) {
        super(core, "gui.filter_button");
        this.gui = gui;
        updateLore();
    }

    public void updateLore() {
        ArrayList<String> lore = new ArrayList<>();
        lore.add(gui.filterType.toUpperCase());
        item.setLore(lore);
    }

    public List<String> getLore() {
        return item.getLore();
    }
}
