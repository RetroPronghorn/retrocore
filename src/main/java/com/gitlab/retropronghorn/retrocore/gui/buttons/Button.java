package com.gitlab.retropronghorn.retrocore.gui.buttons;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Button implements Listener {
    public ItemStack item;
    public String configNode;

    /**
     * Create a new item with a display name
     *
     * @param configNode Config node for the item we're creating
     **/
    public Button(RetroCore core, String configNode) {

        this.configNode = configNode;
        String material = RetroCore.config.getString(configNode + ".material");
        ItemStack item = new ItemStack(Material.getMaterial(material));
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(RetroCore.config.getString(configNode + ".label"));
        item.setItemMeta(meta);
        this.item = item;
        // Register events
        core.getServer().getPluginManager().registerEvents(this, core);
    }

}
