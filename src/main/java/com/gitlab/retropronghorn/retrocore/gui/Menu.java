package com.gitlab.retropronghorn.retrocore.gui;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.gui.buttons.ButtonManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Menu extends Paging {
    private RetroCore core;
    private ArrayList<ItemStack> menu = new ArrayList<>();
    private RetroInventory retroInventory;

    // Buttons
    ItemStack blankSlot;
    /**
     * Create a Menu
     *
     * @param core RetroCore instance
     * @param inventory RetroInventory to add the menu to
     */
    public Menu(RetroCore core, RetroInventory inventory) {
        super(inventory);
        this.core = core;
        this.retroInventory = inventory;

        // Blank slot
        blankSlot = new ItemStack(Material.getMaterial(RetroCore.config.getString("gui.empty_slot.material")));
        ItemMeta meta = blankSlot.getItemMeta();
        meta.setDisplayName(ChatColor.BLACK + "");
        blankSlot.setItemMeta(meta);
    }

    /**
     * Build the inventory with a menu
     *
     * @return returns the inventory with the menu
     */
    public Inventory build() {
        ButtonManager buttonManager = retroInventory.gui.buttonManager;
        for(int i = 0; i < RetroCore.config.getInt("row_size"); i++) {
            menu.add(blankSlot);
        }
        // Add Buttons
        //  Next Button
        if (retroInventory.gui.getOptions().get("enable_paging")) {

            if (retroInventory.page > 1)
                menu.set(RetroCore.config.getInt(buttonManager.nextButton.configNode + ".menu_index"), buttonManager.prevButton.item);

            if (retroInventory.items.size() >= retroInventory.getInventory().getSize())
                menu.set(RetroCore.config.getInt("row_size") - 1, buttonManager.nextButton.item);
        }
        // Close button
        if (retroInventory.gui.getOptions().get("close_button"))
            menu.set(
                    RetroCore.config.getInt(buttonManager.closeButton.configNode + ".menu_index"),
                    buttonManager.closeButton.item);

        // Filter button
        if (retroInventory.gui.getOptions().get("enable_basic_filter"))
            menu.set(
                    RetroCore.config.getInt(buttonManager.filterButton.configNode + ".menu_index"),
                    buttonManager.filterButton.item);

        // Increase inventory size
        int rowSize = RetroCore.config.getInt("row_size");
        Inventory menuInventory = Bukkit.createInventory(
                null,
                retroInventory.getInventory().getSize() + rowSize, retroInventory.gui.name);
        menuInventory.setContents(retroInventory.getInventory().getContents());
        for(int i = 0; i < RetroCore.config.getInt("row_size"); i++) {
            int inventoryIndex = menuInventory.getSize() - rowSize + i;
            int menuIndex = menu.size() - rowSize + i;
            menuInventory.setItem(inventoryIndex, menu.get(menuIndex));
        }

        // Add menu items
        return menuInventory;
    }

}
