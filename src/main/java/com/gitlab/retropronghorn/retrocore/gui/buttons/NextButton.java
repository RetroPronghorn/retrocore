package com.gitlab.retropronghorn.retrocore.gui.buttons;

import com.gitlab.retropronghorn.retrocore.RetroCore;

public class NextButton extends Button {
    /**
     * Create the next button
     *
     * @param core RetroCore instance
     */
    public NextButton(RetroCore core) {
        super(core, "gui.next_button");
    }
}
