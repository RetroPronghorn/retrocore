package com.gitlab.retropronghorn.retrocore.gui.buttons;

import com.gitlab.retropronghorn.retrocore.RetroCore;

public class PrevButton extends Button {
    /**
     * Create Previous Button
     *
     * @param core RetroCore instance
     */
    public PrevButton(RetroCore core) {
        super(core, "gui.prev_button");
    }
}
