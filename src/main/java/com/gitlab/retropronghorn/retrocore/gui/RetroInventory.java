package com.gitlab.retropronghorn.retrocore.gui;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.utils.ItemStackSort;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class RetroInventory {
    protected Inventory inventory;
    protected ArrayList<ItemStack> items;
    public final ArrayList<ItemStack> itemCache;
    protected Integer page = 1;
    public final RetroCore core;
    public final GUI gui;
    public final Menu menu;
    /**
     * Create a new RetroInventory instance
     *
     * @param core Instance to RetroCore
     * @param gui GUI parent
     * @param items Items in this gui
     */
    public RetroInventory(RetroCore core, GUI gui, ArrayList<ItemStack> items) {
        this.core = core;
        this.items = items;
        // Shallow clone item list
        this.itemCache = new ArrayList<ItemStack>(items);
        this.gui = gui;

        inventory = Bukkit.createInventory(null, gui.size, gui.name);

        // ----------------------
        // POPULATE INVENTORY
        // ----------------------
        populate();

        // ----------------------
        // ADD MENU
        // ----------------------
        menu = new Menu(core, this);
        if (gui.getOptions().get("enable_menu"))
            inventory = menu.build();

    }

    /**
     * Get the inventory class
     *
     * @return bukkit inventory class
     **/
    public Inventory getInventory() {
        return this.inventory;
    }

    /**
     * Populate the GUI with items
     **/
    protected void populate() {
        for(int i = 0; i < items.size(); i++) {
            // If we're past the inventory size, and the last item is not null we've overflowed.
            if (i > inventory.getSize() - 1 &&
                    inventory.getItem(inventory.getSize()-1) != null &&
                    !gui.getOptions().get("enable_paging")) {
                RetroCore.log(ChatColor.YELLOW + "Warning, inventory item count overflows inventory size. Consider enabling paging.");
                return;
            }
            inventory.addItem(items.get(i));
        }
    }

    /**
     * Repopulate the GUI with items
     **/
    public void populate(ArrayList<ItemStack> items) {
        this.items = new ArrayList<ItemStack>(items);
        int usableSize = (gui.getOptions().get("enable_menu")) ?
                inventory.getSize() - RetroCore.config.getInt("row_size") : inventory.getSize();

        this.setPage(1);

        for(int i = 0; i < usableSize; i++) {
            // If we're past the inventory size, and the last item is not null we've overflowed.
            if (i > usableSize - 1 || i > items.size() &&
                    inventory.getItem(usableSize-1) != null &&
                    !gui.getOptions().get("enable_paging")) {
                RetroCore.log(ChatColor.YELLOW + "Warning, inventory item count overflows inventory size. Consider enabling paging.");
                return;
            }
            inventory.setItem(i, items.get(i));
        }

        this.menu.recalcMenuNav(menu, items.size() <= usableSize);
    }

    /**
     * Repopulate the GUI with items from a specific page
     *
     * @param page the page to render on the GUI
     **/
    protected void populatePage(List<ItemStack> page) {
        int usableSize = inventory.getSize() - RetroCore.config.getInt("row_size");

        for(int i = 0; i < usableSize; i++) {
            if (i < page.size()) {
                inventory.setItem(i, page.get(i));
            } else {
                inventory.clear(i);
            }
        }
    }

    /**
     * Set current active page on the GUI
     *
     * @param page the page to set on the GUI
     **/
    protected void setPage(Integer page) {
        this.page = page;
    }

    /**
     * Filter items by string type
     *
     * @param filter filter string to sort by
     **/
    public void filterItems(String filter) {
        switch (filter) {
            // Alpha sort
            case "alpha":
                ArrayList<ItemStack> alphaSortedItems = ItemStackSort.alphaSort(this.items);
                this.populate(alphaSortedItems);
                break;
            case "type":
                ArrayList<ItemStack> typeSortedItems = ItemStackSort.typeSort(this.items);
                this.populate(typeSortedItems);
                break;
            case "none":
                this.populate(itemCache);
                break;
            default:
                break;
        }
    }
}
