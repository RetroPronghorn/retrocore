package com.gitlab.retropronghorn.retrocore.throwables.effects;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;

public class HitHandler {

    /**
     * Apply potion effects to a player
     *
     * @param player Player to apply the potion effects to
     * @param effectsMeta Effects meta to read from
     **/
    public void applyEffects(Player player, String effectsMeta) {
        HashMap<String, Integer> effects = new HashMap<String, Integer>();
        String[] splitEffects = effectsMeta.split(",");
        for (String effect : splitEffects) {
            String[] oneEffect = effect.split(":");

            PotionEffect potionEffect = new PotionEffect(
                    PotionEffectType.getByName(oneEffect[0]),
                    Integer.parseInt(oneEffect[1]),
                    1,
                    true);

            player.addPotionEffect(potionEffect);
        }
    }

    /**
     * Play sounds on target player and attacker
     *
     * @param victim Victim to play sounds on
     * @param attacker Attacker to play sounds on
     **/
    public void dispatchSounds(Player victim, Player attacker) {
        Boolean hitSounds = RetroCore.config.getBoolean("throwables.hit.enabled");
        Boolean gothitSounds = RetroCore.config.getBoolean("throwables.gothit.enabled");

        if (hitSounds)
            attacker.playSound(
                    attacker.getLocation(),
                    Sound.valueOf(RetroCore.config.getString("throwables.hit.sound")),
                    1f,
                    1f);

        if (gothitSounds)
            victim.playSound(
                    attacker.getLocation(),
                    Sound.valueOf(RetroCore.config.getString("throwables.gothit.sound")),
                    1f,
                    1f);
    }

    /**
     * Notify both the victim and attacker of events
     *
     * @param victim Victim to notify in chat
     * @param attacker Attacker to notify in chat
     * @param itemName Item name that caused the event
     **/
    public void notifyPlayersInChat(Player victim, Player attacker, String itemName) {
        if (victim.getDisplayName().equals(attacker.getDisplayName())) {
            victim.sendMessage(ChatColor.RED + "You tagged yourself with a " + itemName + "!");
            return;
        }

        victim.sendMessage(ChatColor.YELLOW + attacker.getDisplayName() + " tagged you with a " + itemName + "!");
        attacker.sendMessage(ChatColor.GREEN + "You tagged " + victim.getDisplayName() + " with a " + itemName + "!");
    }

}
