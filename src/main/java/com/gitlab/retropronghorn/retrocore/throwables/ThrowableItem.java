package com.gitlab.retropronghorn.retrocore.throwables;

import com.gitlab.retropronghorn.retrocore.RetroCore;
import com.gitlab.retropronghorn.retrocore.listeners.ThrowableListeners;
import com.gitlab.retropronghorn.retrocore.throwables.entities.ParticleEnabledThrowable;
import com.gitlab.retropronghorn.retrocore.throwables.entities.ThrowableEntity;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class ThrowableItem {
    public final String material = "SNOWBALL";
    public final NamespacedKey throwableIdentifier;
    public NamespacedKey effectsIdentifier;
    public NamespacedKey impactParticlesIdentifier;
    public NamespacedKey trailParticlesIdentifier;
    public final String name;
    public final String description;
    public final RetroCore core;

    /**
     * Create a new throwable item
     *
     * @param core Core instance for reference
     * @param name DisplayName of the throwable item
     * @param description Lore description of the item
     **/
    public ThrowableItem(RetroCore core, String name, String description) {
        this.core = core;
        this.name = name;
        this.description = description;

        this.throwableIdentifier = new NamespacedKey(core, "throwable");
        this.effectsIdentifier = new NamespacedKey(core, "effects");
        this.impactParticlesIdentifier = new NamespacedKey(core, "impact_particles");
        this.trailParticlesIdentifier = new NamespacedKey(core, "trail_particles");

        core.getServer().getPluginManager().registerEvents(new ThrowableListeners(this), core);
    }

    /**
     * Spawn a new throwable item with without particles
     *
     * @param count Integer amount to spawn in the itemstack
     * @param effect Effect to apply to the victim
     * @param duration Duration of the effect
     **/
    public ItemStack spawn(Integer count, String effect, Integer duration) {
        ArrayList<String> particles = new ArrayList<String>();
        return this.spawn(count, effect, duration, particles);
    }

    /**
     * Spawn a new throwable item with
     *
     * @param count Integer amount to spawn in the itemstack
     * @param effects List of effects and duration of the effects
     **/
    public ItemStack spawn(Integer count, HashMap<String, Integer> effects) {
        ArrayList<String> particles = new ArrayList<String>();
        return this.spawn(count, effects, particles);
    }

    /**
     * Spawn a new throwable item with
     *
     * @param count Integer amount to spawn in the itemstack
     * @param effect Effect to apply when hit
     * @param duration How long the effect should last when activated
     * @param particles list of particles to apply to the victim
     **/
    public ItemStack spawn(Integer count, String effect, Integer duration, ArrayList<String> particles) {
        // Create meta HashMap
        HashMap<String, String> meta = new HashMap<String, String>();
        meta.put("material", this.material);
        meta.put("name", this.name);
        meta.put("description", this.description);
        // Put effect into HashMap
        HashMap<String, Integer> effects = new HashMap<String, Integer>();
        effects.put(effect, duration);

        if (!particles.isEmpty())
            return (new ParticleEnabledThrowable(this, meta, effects, particles, count)).getItemStack();
        return (new ThrowableEntity(this, meta, effects, count)).getItemStack();
    }

    /**
     * Spawn a new throwable item with particles
     *
     * @param count Integer amount to spawn in the itemstack
     * @param effects List of the effects, and duration of the effects
     * @param particles list of particle names to apply when a user is hit
     **/
    public ItemStack spawn(Integer count, HashMap<String, Integer> effects, ArrayList<String> particles) {
        // Create MetaHashmap
        HashMap<String, String> meta = new HashMap<String, String>();
        meta.put("material", this.material);
        meta.put("name", this.name);
        meta.put("description", this.description);

        if (!particles.isEmpty())
            return (new ParticleEnabledThrowable(this, meta, effects, particles, count)).getItemStack();
        return (new ThrowableEntity(this, meta, effects, count)).getItemStack();
    }
}
