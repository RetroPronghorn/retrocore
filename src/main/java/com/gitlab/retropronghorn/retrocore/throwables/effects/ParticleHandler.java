package com.gitlab.retropronghorn.retrocore.throwables.effects;


import com.gitlab.retropronghorn.retrocore.RetroCore;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class ParticleHandler {

    public void particleTrail(Entity entity) {
        //entity.getLocation();
    }

    /**
     * Activate new particles on a player when hit
     *
     * @param core Reference to the RetroCore plugin class
     * @param player Player to particle effects to
     * @param particles Particles to spawn on the player
     **/
    public void playerHitParticle(RetroCore core, Player player, String particles) {
        // Duration * 4 (5 ticks) = 20 Ticks aka 1 second
        int repetitions = core.getConfig().getInt("throwables.particle_duration") * 4;
        iterator(core, player, repetitions, particles);
    }


    /**
     * Spawn the particles into the victims world
     *
     * @param player Player to apply the particles to
     * @param particles Particles to apply to the victim
     **/
    public void playParticleStack(Player player, String particles) {
        Location loc = player.getLocation();
        String[] particlesArray = particles.split(",");
        // Play all particles in the stack
        for (String particle : particlesArray) {
            player.getWorld().playEffect(loc, Effect.valueOf(particle), 0);
        }
    }


    /**
     * Iterate x amount of times and apply particles to victim
     *
     * @param core Reference to the RetroCore plugin class
     * @param player Player to apply effects to
     * @param reps repititions to iterate
     * @param particles particles to apply to the victim
     **/
    private void iterator(RetroCore core, Player player, Integer reps, String particles) {
        // Schedule particle effect in 5 ticks.
        core.getServer().getScheduler().scheduleSyncDelayedTask(core, new Runnable() {
            public void run() {
                playParticleStack(player, particles);
                if (reps > 0)
                    iterator(core, player,reps - 1, particles);
            }
        }, 5);
    }
}
