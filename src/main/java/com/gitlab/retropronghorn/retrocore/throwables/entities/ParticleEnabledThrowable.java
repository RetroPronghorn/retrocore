package com.gitlab.retropronghorn.retrocore.throwables.entities;

import org.bukkit.NamespacedKey;
import org.bukkit.event.Listener;
import com.gitlab.retropronghorn.retrocore.throwables.ThrowableItem;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParticleEnabledThrowable extends ThrowableEntity implements Listener {
    ArrayList<String> particles;

    /**
     * Create a new throwable entity
     *
     * @param instance Instance of the throwable item class
     * @param meta Metadata to attach to the throwable entity
     * @param effects Effects to apply on contact with victim
     * @param particles Particle effects to apply on contact with victim
     * @param count Amount of the throwable entity to add to the stack
     **/
    public ParticleEnabledThrowable(ThrowableItem instance, HashMap<String, String> meta, HashMap<String, Integer> effects, ArrayList<String> particles, Integer count) {
        super(instance, meta, effects, count);
        this.particles = particles;

        this.registerImpactParticles();
    }

    public void registerImpactParticles() {
        String particlesMeta = "";
        for (String particle : this.particles) {
            particlesMeta += particle + ",";
        }
        particlesMeta = particlesMeta.substring(0, particlesMeta.length() - 1);

        NamespacedKey key = this.instance.impactParticlesIdentifier;
        ItemMeta itemMeta = this.item.getItemMeta();
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, particlesMeta);
        this.item.setItemMeta(itemMeta);
    }
}
