package com.gitlab.retropronghorn.retrocore.utils;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Comparator;

public class ItemStackSort {
    /**
     * Sort ItemStack array by item displayname alphabetical order
     *
     * @param items ItemStack array to sort
     **/
    public static ArrayList<ItemStack> alphaSort(ArrayList<ItemStack> items) {
        ArrayList<ItemStack> alphaSortedItems = items;
        alphaSortedItems.sort(new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack item2, ItemStack item1)
            {

                return  item2.getItemMeta().getDisplayName().compareTo(item1.getItemMeta().getDisplayName());
            }
        });
        return alphaSortedItems;
    }

    /**
     * Sort ItemStack array by item type
     *
     * @param items ItemStack array to sort
     **/
    public static ArrayList<ItemStack> typeSort(ArrayList<ItemStack> items) {
        ArrayList<ItemStack> typeSortedItems = items;
        typeSortedItems.sort(new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack item2, ItemStack item1)
            {

                return item2.getType().toString().compareTo(item1.getType().toString());
            }
        });
        return typeSortedItems;
    }
}
